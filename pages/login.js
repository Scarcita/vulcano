import Head from 'next/head'
import Image from 'next/image'
import React, {useState} from 'react'

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'


import Link from 'next/link'
import { useRouter } from 'next/router'
import { signIn} from 'next-auth/react'

export default function Login() {

    const validationSchema = Yup.object().shape({
        username: Yup.string()
        .required('Username is required'),
        password: Yup.string()
        .min(6, "Passaword must be at least 6 characters")
        .required("Password is required")
    })

    const formOptions = { resolver: yupResolver(validationSchema)};
    const {register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
        return false;
    }

    ///////////////////

  const router = useRouter()
  const [authState, setAuthState] = useState({
    username: '',
    password: ''
  })
  const [pageState, setPageState] = useState({
    error: '',
    processing: false
  })
  const handleFielChange = (e) => {
    setAuthState(old => ({ ...old, [e.target.id]: e.target.value}))
  }

  const handleAuth = async () => {
    setPageState(old => ({...old, processing: true, error: ''}))
    signIn('credentials', {
      ...authState,
      redirect: false
    }).then(response => {
      console.log(response)
      if (response.ok) {
        router.push("/components/Desktop/nuevaOrden")
      }else {
        setPageState(old => ({ ...old, processing: false, error: response.error}))
      }
    }).catch(error => {
      console.log(error)
      setPageState(old => ({...old, processing: false, error: error.message ?? "Something went wrong!"}))
    })
  }

  return (
    <div className="w-screen h-screen flex flex-row">
   
        <div className="flex flex-row">
            <div className="w-[866px] flex flex-col justify-center items-center bg-blue">

            </div>

            <div className="w-[500px] flex flex-col justify-center items-center">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="w-[340px] h-full">
                        <label htmlFor="first" className="text-[14px] mb-1 text-[#3A4567]">Usuario</label>
                        <input id="username" name="username" type='text' {...register('username')} className="w-[340px] h-[48px] bg-[#F6F6FA] rounded-[7px] border-[1px] border-[#E4E7EB] mb-3 px-3" 
                        placeholder='Username' 
                        value={authState.username}
                        onChange={handleFielChange} />
                        <div className="bg-[#EE002B]">{errors.username?.message}</div>
                        <label htmlFor="last" className="text-[14px] mb-1 text-[#3A4567]">Password</label>
                        <input id="password" name="password" {...register('password')} className="w-[340px] h-[48px] rounded-[7px] bg-[#F6F6FA] border-[1px] border-[#E4E7EB] mb-3 px-3"
                        placeholder='*******' 
                        value={authState.password}
                        onChange={handleFielChange} />
                        <div className="bg-[#EE002B]">{errors.password?.message}</div>
                        <button className="w-[340px] h-[48px] rounded-[7px] bg-[#3682F7] border-0 text-white text-[14px"
                        disabled={pageState.processing} 
                        type="submit" 
                        onClick={handleAuth}>
                                Ingresar  
                        </button>
                    </div>
            </form>

            </div>
        </div>

    </div>
  )
}


