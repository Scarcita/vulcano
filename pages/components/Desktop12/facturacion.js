import React, { useEffect, useState } from 'react';
import Head from "next/head"
import Image from 'next/image'
import GaugeChart from "react-gauge-chart";
import ListOTsCerradas from './listOTsCerradas';
import BarData from './BarChartFacturacion';
import BarData2 from './BarChartWork';

import ImgRepuestos from '../../../public/Desktop12/repuestos.svg'
import ImgTable from '../../../public/Desktop12/tablero.svg'
import ImgCosto from '../../../public/Desktop12/costo.svg'


export default function Facturacion() {
      
    return (
        <div className='flex h-full flex-col'>
            <div className='w-[1024px] mt-[36px] ml-[40px]'>
                <div className='w-[1024px] mt-[36px]'>
                    <div className='flex flex-row'>
                        <h2 className="text-[24px] font-semibold">Facturacion</h2>
                        <h2 className="text-[24px]">/Mes actual</h2>
                    </div>
                </div>
                <div className="mt-[20px] w-[1024px]">
                    <div className="flex flex-col">
                        <div className="flex flex-row">
                            <div className="w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px]">
                                <div className="flex flex-row">
                                    <div className="w-[169px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">#servicios Requeridos</text>
                                        <div className="flex flex-row">
                                            <text className="text-[34px] text-[#3682F7] pl-[10px]">3490</text>
                                            <div className=' flex flex-row w-[44px] h-[20px] bg-[#3682F7] rounded-[5px] mt-[17px] ml-[8px] '>
                                                <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[6px] ml-[3px]'>
                                                    {/* <Image
                                                        src={Imgup}
                                                        layout='fixed'
                                                        alt='Imgup'
                                                    /> */}
                                                </div>
                                                <text className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">15%</text>
                                            </div>
                                        </div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Este mes</h3>
                                    </div>
                                    <div className=" h-[64px] bg-[#FFFFFF]">
                                        <BarData/>
                                    </div> 
                                </div> 
                            </div>
                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px]">
                                <div className="flex flex-row">
                                    <div className="w-[169px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">#partes</text>
                                        <div className="flex flex-row">
                                            <text className="text-[34px] text-[#3682F7] pl-[10px]">3490</text>
                                            <div className=' flex flex-row w-[44px] h-[20px] bg-[#3682F7] rounded-[5px] mt-[17px] ml-[8px] '>
                                                <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[6px] ml-[3px]'>
                                                    {/* <Image
                                                        src={Imgup}
                                                        layout='fixed'
                                                        alt='Imgup'
                                                    /> */}
                                                </div>
                                                <text className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">15%</text>
                                            </div>
                                        </div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Este mes</h3>
                                    </div>
                                    <div className=" h-[64px] bg-[#FFFFFF]">
                                        <BarData/>
                                    </div> 
                                </div> 
                            </div>
                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md">
                                <div className="flex flex-row">
                                    <div className="w-[169px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">#Trabajos externos</text>
                                        <div className="flex flex-row">
                                            <text className="text-[34px] text-[#3682F7] pl-[10px]">3490</text>
                                            <div className=' flex flex-row w-[44px] h-[20px] bg-[#3682F7] rounded-[5px] mt-[17px] ml-[8px] '>
                                                <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[6px] ml-[3px]'>
                                                    {/* <Image
                                                        src={Imgup}
                                                        layout='fixed'
                                                        alt='Imgup'
                                                    /> */}
                                                </div>
                                                <text className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">15%</text>
                                            </div>
                                        </div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Este mes</h3>
                                    </div>
                                    <div className=" h-[64px] bg-[#FFFFFF]">
                                        <BarData/>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                        <div className="flex flex-row">
                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px] mt-[24px]">
                                <div className="flex flex-row">
                                    <div className="w-[166px]">
                                        <text className="text-[14px] text-[#000000]pl-[10px]">Ventas-Servicios Requeridos</text>
                                        <div className="text-[34px] text-[#3682F7] pl-[10px]">3490</div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">actual</h3>
                                    </div>
                                    <div className="w-[165px] pl-[70px]">
                                        <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full pl-[15px] pt-[16px] mt-[15px]">
                                            <Image
                                            src={ImgRepuestos}
                                            layout='fixed'
                                            alt='ImgRepuestos'
                                            /> 
                                        </div> 
                                    </div>
                                    
                                </div> 
                            </div>

                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px] mt-[24px]">
                                <div className="flex flex-row">
                                    <div className="w-[166px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">Ventas Partes</text>
                                        <div className="text-[34px] text-[#3682F7] pl-[10px]">3490</div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Actual</h3>
                                    </div>
                                    <div className="w-[165px]  pl-[70px] ">
                                        <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full pl-[15px] pt-[16px] mt-[15px]">
                                            <Image
                                            src={ImgRepuestos}
                                            layout='fixed'
                                            alt='ImgRepuestos'
                                            /> 
                                        </div> 
                                    </div>
                                    
                                </div> 
                            </div>

                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px] mt-[24px]">
                                <div className="flex flex-row">
                                    <div className="w-[166px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">Ventas-Trabajos externos</text>
                                        <div className="flex flex-row">
                                            <h1 className="text-[34px] text-[#A5A5A5] pl-[10px]">12/</h1>
                                            <h1 className="text-[34px] text-[#3682F7]">50</h1>
                                        </div>      
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Codificadas vs Solicitudes</h3>
                                    </div>
                                    <div className="w-[165px]  pl-[70px] ">
                                        <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full pl-[15px] pt-[16px] mt-[15px]">
                                            <Image
                                            src={ImgRepuestos}
                                            layout='fixed'
                                            alt='ImgRepuestos'
                                            /> 
                                        </div> 
                                    </div>
                                    
                                </div> 
                            </div>

                        </div>
                        
                    </div>
                </div>
                <div className="w-[1024px] mt-[26px]">
                    <div className='flex flex-row mt-[15px] '>
                        <div>
                            <div className='flex flex-row'>
                                <h2 className="text-[24px] font-semibold">Facturacion</h2>
                                <h2 className="text-[24px]">/Mes Actual vs Anterior</h2>
                            </div>
                            <div className='flex flex-row mt-[15px] '>
                                <div>
                                    <div className='flex flex-row w-[338px] h-[100px] bg-white rounded-[16px] shadow-md text-center justify-around'>
                                        <div>
                                            <div className="text-[14px] text-black ">Total ventas</div>
                                            <div className="text-[40px] text-blue">3400</div>
                                            <div className="text-[10px] text-[#A5A5A5]">Mes anterior</div>
                                        </div>
                                        <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] mt-[24px]">
                                            <Image
                                            src={ImgCosto}
                                            layout='fixed'
                                            alt='ImgCosto'
                                            /> 
                                        </div>
                                    </div>
                                    <div className='flex flex-row mt-[17px]  w-[338px] h-[100px] bg-white rounded-[16px] text-center shadow-md text-center justify-around'>
                                        <div>
                                            <div className="text-[14px] text-black ">Total ventas</div>
                                            <div className="text-[40px] text-blue">3400</div>
                                            <div className="text-[10px] text-[#A5A5A5]">Mes anterior</div>
                                        </div>
                                        <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] mt-[24px] ">
                                            <Image
                                            src={ImgCosto}
                                            layout='fixed'
                                            alt='ImgCosto'
                                            /> 
                                        </div>
                                    </div>
                                </div>
                                <div
                                    className="bg-[#FFFF] w-[338px] h-[219px] flex flex-col rounded-[24px] justify-center items-center text-[#000000] ml-[15px]"
                                    >
                                    <GaugeChart
                                        nrOfLevels={100}
                                        arcsLength={[75]}
                                        colors={["#71AD46"]}
                                        percent={0.75}
                                        arcPadding={0.10}
                                        // className="w-[90pcx] h-[80px] text-[#71AD46] "
                                    />
                                    <p className="mt-5 ">
                                        Mes actual vs anterior
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <div className='ml-[15px]'> 
                            <div className='flex flex-row'>
                                <h2 className="text-[24px] font-semibold">OTs cerradas</h2>
                                <h2 className="text-[14px] mt-[10px]">/Control de calidad realizado</h2>
                            </div>
                        
                            <ListOTsCerradas/>
                        </div>
                    </div>

                    
                </div>
                <div className="w-[1024px] mt-[26px]">
                    <div className='flex flex-row'>
                        <h2 className="text-[24px] font-semibold">Ordenes de trabajo</h2>
                        <h2 className="text-[24px]">/Mes Actual vs Anterior</h2>
                    </div>
                    <div className=" flex flex-row  mt-[15px] ">
                        <div>
                            <div className='flex flex-row w-[228px] h-[86px] bg-white rounded-[16px] justify-between shadow-md text-center pl-[20px]'>
                                <div>
                                    <div className="text-[40px] text-blue font-semibold">3400</div>
                                    <div className="text-[10px] text-[#A5A5A5]">Mes anterior</div>
                                </div>
                                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[15px] mt-[18px] mr-[20px]">
                                    <Image
                                    src={ImgCosto}
                                    layout='fixed'
                                    alt='ImgCosto'
                                    /> 
                                </div>
                            </div>
                            <div className='flex flex-row mt-[17px]  w-[228px] h-[86px] bg-white rounded-[16px] justify-between text-center shadow-md text-center pl-[20px]'>
                                <div>
                                    <p className="text-[40px] text-blue font-semibold">3400</p>
                                    <p className="text-[10px] text-[#A5A5A5]">Mes anterior</p>
                                </div>
                                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[14px] mt-[18px] mr-[20px] ">
                                    <Image
                                    src={ImgCosto}
                                    layout='fixed'
                                    alt='ImgCosto'/> 
                                </div>
                            </div>
                        </div>
                        <div
                            className="bg-[#FFFF] w-[285px] h-[185px] flex flex-col rounded-[24px] justify-center items-center text-[#000000] ml-[15px] "
                            >
                            <GaugeChart
                                nrOfLevels={100}
                                arcsLength={[75]}
                                colors={["#71AD46"]}
                                percent={0.75}
                                arcPadding={0.10}
                                // className="w-[90pcx] h-[80px] text-[#71AD46] "
                            />
                            <p className="mt-5 ">
                                Mes actual vs anterior
                            </p>
                        </div>
                        <div className="flex flex-row">
                            <div className="w-[150px] text-center">
                                <div>
                                    <div className='ml-[50px] flex flex-row w-[40px] h-[18px] bg-[#3682F7] rounded-[5px] '>
                                        <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[5px] ml-[3px]'>
                                            {/* <Image
                                                src={Imgup}
                                                layout='fixed'
                                                alt='Imgup'
                                            /> */}
                                        </div>
                                        <text className="text-[10px] text-[#FFFFFF] font-semibold ml-[3px] mt-[1px]">15%</text>
                                    </div>
                                    <h2 className="text-[32px]">3490</h2>
                                    <h2 className="text-[11px]">OTs creadas</h2>
                                </div> 

                                <div>
                                    <div className='ml-[50px] mt-[20px] flex flex-row w-[40px] h-[18px] bg-[#EE002B] rounded-[5px]'>
                                        <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[5px] ml-[3px]'>
                                                {/* <Image
                                                    src={Imgup}
                                                    layout='fixed'
                                                    alt='Imgup'
                                                    />  */}
                                        </div>
                                        <text className="text-[10px] text-[#FFFFFF] font-semibold ml-[3px] mt-[1px]">15%</text>
                                    </div>
                                    <h2 className="text-[32px]">70</h2>
                                    <h2 className="text-[11px]">OTs desde citas</h2>
                                </div> 
                            </div>
                            <div className="w-[340px] h-[182px] bg-[#FFFFFF] rounded-[24px] pt-[10px] pl-[5px] pr-[5px]">
                                <BarData2/>

                            </div> 

                        </div>

                    </div>

                </div>
            </div>

        </div>
)}



