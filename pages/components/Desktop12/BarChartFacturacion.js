import React, { useEffect, useState } from 'react';


import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';
  import { Bar } from 'react-chartjs-2';
  
  ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
  );

export default function BarData() {

///////////// BAR CHART UNO///////
    const options = {
        responsive: true,
        plugins: {
          legend: {
            display: false,
            position: "top",
          },
          title: {
            display: false,
            text: '',
          },
        },
      };
      
      const labels = ['', '', '', '', '', '', '', '', '', '', '', ''];
      
       const dataGraf = {
        labels,
        datasets: [
          {
            label: 'Dataset 1',
            data: [17, 7, 8, 16, 22, 17, 11, 18, 23, 8, 7, 25],
            backgroundColor: '#3682F7',
          },
        ],
      };

     

return(
    <div>
        <Bar options={options} data={dataGraf} width={140} height={100}/>
    </div>

)
}





