import React, { useEffect, useState } from 'react';


import { Bar } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';


ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export default function BarChart() {

    const options = {
        responsive: true,
        plugins: {
          legend: {
            display: false,
            position: "top",
          },
          title: {
            display: false,
            text: 'Chart.js Bar Chart',
          },
        },
      };
      
      const labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      
       const dataGraf = {
        labels,
        datasets: [
          {
            label: 'Dataset 1',
            data: [17, 7, 8, 16, 22, 17, 11, 18, 23, 8, 7, 25],
            backgroundColor: '#3682F7',
          },
        ],
      };



    return (
        <div>
            <Bar options={options} data={dataGraf} width={200} height={100}/>
        </div>

    )
}