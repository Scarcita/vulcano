import React, { useEffect, useState } from 'react';
import GaugeChart from "react-gauge-chart";



export default function GaugeCharts() {

    return (
        <div>
             <GaugeChart
                            nrOfLevels={100}
                            arcsLength={[75]}
                            colors={["#71AD46"]}
                            percent={0.75}
                            arcPadding={0.10}
                            // className="w-[90pcx] h-[80px] text-[#71AD46] "
                        />
                        <p className="mt-5 ">
                            Mes actual vs anterior
                        </p>
        </div>

    )
}