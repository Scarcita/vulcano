import React, { useEffect, useState } from 'react';
import Head from 'next/head'
import GaugeCharts from './gaugeChart';
import Image from 'next/image';
import ListOTs from './list.OTs';
import BarChart from './bar';

///////////// IMAGENES //////////
import ImgCheck from '../../../public/Desktop5/check.svg'
import ImgCheck1 from '../../../public/Desktop5/check1.svg'
import Imgup from '../../../public/Desktop5/up.svg'
import ImgStop from '../../../public/Desktop5/stop.svg'
import ImgPlay from '../../../public/Desktop5/play.svg'
import ImgTiket from '../../../public/Desktop5/tiket.svg'
import ImgTwoTiket from '../../../public/Desktop5/twoTiket.svg'
import ImgBandera from '../../../public/Desktop5/bandera.svg'
import ImgStop1 from '../../../public/Desktop5/stop1.svg'
import ImgPlay1 from '../../../public/Desktop5/play1.svg'
import ImgTiket1 from '../../../public/Desktop5/tiket1.svg'
import ImgTwoTiket1 from '../../../public/Desktop5/twoTiket1.svg'
import ImgBandera1 from '../../../public/Desktop5/bandera1.svg'
import ImgTable from '../../../public/Desktop5/tablero.svg'


export default function WorkOrder() {

    return (
      <div className='flex h-full flex-col'>
        <div className="flex flex-row">
            <div className='ml-[75px] mt-[78px] w-[585px]'>
                <div className='flex flex-row justify-between'>
                    <div className='flex flex-row'>
                        <h2 className="text-[24px] font-semibold">Ordenes de trabajo</h2>
                        <h2 className="text-[24px]">/Global</h2>
                    </div>
                    <button className="w-[67px] h-[20px]  border-[1px] border-[#3682F7] rounded-[19px] text-blue hover:bg-blue hover:text-white text-[11px]"   
                    > Nueva</button>
                </div>
                <div className="flex flex-row mt-[14px]">
                    <div className=" w-[186px] h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px]  mr-[20px]">
                        <div className="flex flex-row pt-[20px]">
                            <span className="text-[15px] text-[15px] mr-[72px] ">Abirtas</span>
                            <Image
                                src={ImgCheck}
                                layout='fixed'
                                alt='ImgCheck'
                            
                            />
                        </div> 
                        <h1 className="text-[25px] text-[45px]">3490</h1>
                    </div>

                    <div className=" w-[186px] h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px]  mr-[20px]">
                        <div className="flex flex-row pt-[20px] ">
                            <span className="text-[15px] text-[15px] mr-[72px] ">Abirtas</span>
                            <Image
                                src={ImgPlay}
                                layout='fixed'
                                alt='ImgPlay'
                            
                            />
                        </div> 
                        <h1 className="text-[25px] text-[45px]">3490</h1>
                    </div>

                    <div className=" w-[186px] h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px] ">
                        <div className="flex flex-row pt-[20px]">
                            <span className="text-[15px] text-[15px] mr-[72px] ">Abirtas</span>
                            <Image
                                src={ImgStop}
                                layout='fixed'
                                alt='ImgStop'
                            
                            />
                        </div> 
                        <h1 className="text-[25px] text-[45px]">3490</h1>
                    </div>

                </div>

                <div className="flex flex-row mt-[14px]">
                    <div className=" w-[186px] h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px]  mr-[20px]">
                        <div className="flex flex-row pt-[20px] ">
                            <span className="text-[15px] text-[15px] mr-[72px] ">Abirtas</span>
                            <Image
                                src={ImgTiket}
                                layout='fixed'
                                alt='ImgTiket'
                            
                            />
                        </div> 
                        <h1 className=" text-[45px]">3490</h1>
                    </div>

                    <div className=" w-[186px] h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px]  mr-[20px]">
                        <div className="flex flex-row pt-[20px] ">
                            <span className="text-[15px] text-[15px] mr-[72px] ">Abirtas</span>
                            <Image
                                src={ImgTwoTiket}
                                layout='fixed'
                                alt='ImgTwoTiket'
                            
                            />
                        </div> 
                        <h1 className=" text-[45px]">3490</h1>
                    </div>

                    <div className=" w-[186px] h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px] ">
                        <div className="flex flex-row pt-[20px]">
                            <span className="text-[15px] text-[15px] mr-[72px] ">Abirtas</span>
                            <Image
                                src={ImgBandera}
                                layout='fixed'
                                alt='ImgBandera'
                            
                            />
                        </div> 
                        <h1 className="text-[45px]">3490</h1>
                    </div>
                </div>
            

                <div className='mt-[40px] '>
                    <div className='flex flex-row'>
                        <h2 className="text-[24px] font-semibold">Ordenes de trabajo</h2>
                        <h2 className="text-[24px]">/Hoy</h2>
                    </div>
                    <div className="flex flex-row">
                        <div className="w-[150px] mt-[45px]">
                            <h2 className="text-[24px] ">Ingresadas</h2>
                            <h2 className="text-[90px] font-semibold">77</h2>
                        </div>
                        <div className="flex-col">
                            <div className="flex flex-row mt-[14px] ml-[20px]">
                                <div className=" w-[128px] h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px] mr-[35px]">
                                    <div className="flex flex-row pt-[13px]">
                                        <span className="text-[12px] mr-[40px]">Abirtas</span>
                                        <Image
                                            src={ImgCheck1}
                                            layout='fixed'
                                            alt='ImgCheck1'
                                        
                                        />
                                    </div> 
                                    <h1 className="text-[30px]">3490</h1>
                                </div>

                                <div className=" w-[128px] h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px] mr-[35px] ">
                                    <div className="flex flex-row pt-[13px]">
                                        <span className="text-[12px] mr-[40px]">Abirtas</span>
                                        <Image
                                            src={ImgPlay1}
                                            layout='fixed'
                                            alt='ImgPlay1'
                                        
                                        />
                                    </div> 
                                    <h1 className="text-[30px]">3490</h1>
                                </div>

                                <div className=" w-[128px] h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px] ">
                                    <div className="flex flex-row pt-[13px]">
                                        <span className="text-[12px] mr-[40px]">Abirtas</span>
                                        <Image
                                            src={ImgStop1}
                                            layout='fixed'
                                            alt='ImgStop1'
                                        
                                        />
                                    </div> 
                                    <h1 className="text-[30px]">3490</h1>
                                </div>
                            </div>

                            <div className="flex flex-row mt-[14px] ml-[20px]">
                                <div className=" w-[128px] h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px] mr-[35px]">
                                    <div className="flex flex-row pt-[13px]">
                                        <span className="text-[12px] mr-[40px]">Abirtas</span>
                                        <Image
                                            src={ImgTiket1}
                                            layout='fixed'
                                            alt='ImgTiket1'
                                        
                                        />
                                    </div> 
                                    <h1 className="text-[30px]">3490</h1>
                                </div>

                                <div className=" w-[128px] h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px] mr-[35px] ">
                                    <div className="flex flex-row pt-[13px]">
                                        <span className="text-[12px] mr-[40px]">Abirtas</span>
                                        <Image
                                            src={ImgTwoTiket1}
                                            layout='fixed'
                                            alt='ImgTwoTiket1'
                                        
                                        />
                                    </div> 
                                    <h1 className="text-[30px]">3490</h1>
                                </div>

                                <div className=" w-[128px] h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md pl-[20px] ">
                                    <div className="flex flex-row pt-[13px]">
                                        <span className="text-[12px] mr-[40px]">Abirtas</span>
                                        <Image
                                            src={ImgBandera1}
                                            layout='fixed'
                                            alt='ImgBandera1'
                                        
                                        />
                                    </div> 
                                    <h1 className="text-[30px]">3490</h1>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>

                <div className='mt-[40px] '>
                    <div className='flex flex-row'>
                        <h2 className="text-[24px] font-semibold">Ordenes de trabajo</h2>
                        <h2 className="text-[24px]">/Año</h2>
                    </div>
                    <div>
                        <div className="flex flex-row">
                            <div className="w-[150px] text-center mt-[32px]">
                                <div>
                                    <div className='ml-[50px] flex flex-row w-[49px] h-[21px] bg-[#3682F7] rounded-[5px] '>
                                        <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[6px] ml-[3px]'>
                                            {/* <Image
                                                src={Imgup}
                                                layout='fixed'
                                                alt='Imgup'
                                            /> */}
                                        </div>
                                        <text className="text-[12px] text-[#FFFFFF] font-semibold ml-[6px]">15%</text>
                                    </div>
                                    <h2 className="text-[40px]">3490</h2>
                                    <h2 className="text-[14px]">OTs creadas</h2>
                                </div> 

                                <div>
                                    <div className='ml-[50px] mt-[20px] flex flex-row w-[49px] h-[21px] bg-[#EE002B] rounded-[5px]'>
                                        <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[6px] ml-[3px]'>
                                                {/* <Image
                                                    src={Imgup}
                                                    layout='fixed'
                                                    alt='Imgup'
                                                    />  */}
                                        </div>
                                        <text className="text-[12px] text-[#FFFFFF] font-semibold ml-[6px]">15%</text>
                                    </div>
                                    <h2 className="text-[40px]">70</h2>
                                    <h2 className="text-[14px]">OTs desde citas</h2>
                                </div> 
                            </div>
                            <div className="w-[450px] h-[240px] bg-[#FFFFFF] rounded-[24px] mt-[5px] pt-[10px] pl-[5px] pr-[5px]">
                                <BarChart/>
                            </div> 

                        </div>
                    </div>
                </div>
            </div>

            <div className='w-[345px] ml-[60px] mt-[80px] mr-[30px]'>
                <div>
                    <div className='flex flex-row'>
                        <h2 className="text-[24px] font-semibold">OTs Finalizadas</h2>
                        <h2 className="text-[14px] mt-[10px]">/En espera de control de calidad</h2>
                    </div>
                    
                    <div className='mt-[14px]'> 
                        <ListOTs/>
                    </div> 
                </div>

                <div>
                    <div className='flex flex-row mt-[40px]'>
                        <h2 className="text-[24px] font-semibold">Ordenes de Trabajo</h2>
                        <h2 className="text-[14px] mt-[10px]">/Mes</h2>
                    </div>
                    <div className='flex flex-row mt-[15px] ml-[52px] w-[227px] h-[86px] bg-white rounded-[16px] justify-evenly text-center shadow-md '>
                        <div>
                            <div className="text-[40px] text-blue">3400</div>
                            <div className="text-[14px] text-blue">Mes anterior</div>
                        </div>
                        <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] mt-[15px]">
                            <Image
                            src={ImgTable}
                            layout='fixed'
                            alt='ImgTable'
                            /> 
                        </div>
                    </div>
                    <div className='flex flex-row mt-[12px] ml-[52px] w-[227px] h-[86px] bg-white rounded-[16px] justify-evenly text-center shadow-md '>
                        <div>
                            <div className="text-[40px] text-blue">3400</div>
                            <div className="text-[14px] text-blue">Mes anterior</div>
                        </div>
                        <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] mt-[15px]">
                            <Image
                            src={ImgTable}
                            layout='fixed'
                            alt='ImgTable'
                            /> 
                        </div>
                    </div>       
                </div>

                <div
                    className="bg-[#FFFF] w-[345px] h-[242px] flex flex-col rounded-[24px] justify-center items-center text-[#000000] mt-[80px] "
                    >
                        <GaugeCharts/>
                       
                </div>
            </div>

        </div>

      </div>
      
    );
  }


