import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'

import ImgLogoFord from '../../../public/Desktop8/logoFord.svg'


export default function ListRepuestos() {
    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState([]);

    useEffect(() => {
  
        setTimeout(function(){
          fetch('https://api.npms.io/v2/search?q=react')
            .then(response => response.json())
            .then(data => setTotal(data.total))
            .then(setShowDots(false));
        }, 2000);
        
      }, [])
    
 /////////////// TABLA DOS ///////////


 const tableSolicitud = [
    {
        id: 0,
        estado: 'Finalizado',
        title: 'AMORTIGUADOR DELANTERO IZQUIERDO ECOSPORT',
        marca: 'Ford',
        details: 'FIESTASE',
        placa: '27349SFD',
        type: 'ford'
    },

    {
        id: 1,
        estado: 'Finalizado',
        title: 'AMORTIGUADOR DELANTERO IZQUIERDO ECOSPORT',
        marca: 'Ford',
        details: 'FIESTASE',
        placa: '27349SFD',
        type: 'ford'
    },

    {
        id: 2,
        estado: 'Finalizado',
        title: 'AMORTIGUADOR DELANTERO IZQUIERDO ECOSPORT',
        marca: 'Ford',
        details: 'FIESTASE',
        placa: '27349SFD',
        type: 'ford'
    },


    ];

    return (
        showDots ? 
        <div className='flex justify-center items-center w-[500px] mt-[40px]'  > 
          <Spinner color="#bebebe" size={17} speed={1} animating={true} style={{marginLeft: 'auto', marginRight: 'auto'}} />
        </div> 
        :
        <div>
            <Table2 tableSolicitud={tableSolicitud}/>
            <button className="w-[500px] h-[35px] bg-blue text-[#FFFFFF] border rounded-b-[24px] hover:bg-white hover:text-blue hover:border-[1px] hover:border-blue">Ver todo</button>
        </div>
    )
}


///////////////// TABLE DOS //////////

const Table2 = (props) => { 

    const { tableSolicitud } = props;
    
        return ( 
    
            <table className={`w-[500px] h-[247px] bg-[#FFFFFF] rounded-t-[24px]`}> 
                <tbody> 
                    <div className="grid grid-cols-1 divide-y w-[490px] pl-3">
                        {tableSolicitud.map(row => <TableRow2 row={row} />)} 
                    </div>
                </tbody>
            </table> 
    
        ); 
    };
    
    
    
        
class TableRow2 extends React.Component{ 

    render() { 

        let row = this.props.row; 

        return ( 
            <div className="flex flex-row">
                <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full mt-[16px] pt-[9px] pl-[9px]">
                    { 
                        row.type === 'ford'?
                        <Image
                        src={ImgLogoFord}
                        layout='fixed'
                        alt='ImgLogoFord'
                        /> 
                        : <></>
                    }
                </div>
                <div className="pl-[10px] pt-[20px] pb-[15px]">
                    <div className="w-[53px] h-[13px] bg-[#71AD46] rounded-[13px] pl-[5px]">
                        <tr className="text-[8px] text-[#FFFFFF]">{row.estado}</tr>
                    </div>
                    <tr className="text-[14px] text-[#000000] font-bold">{row.title}</tr>
                    <tr className="text-[8px] text-[#000000]"> {row.marca} *{row.details} *{row.placa}</tr>
                </div>
                
            </div>
        ) 

    } 
} 


