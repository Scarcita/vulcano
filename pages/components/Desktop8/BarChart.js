import React, { useEffect, useState } from 'react';



import { Bar } from 'react-chartjs-2';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';

  
  ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
  );

export default function BarChartGlobal() {

    const options = {
        responsive: true,
        plugins: {
          legend: {
            display: false,
            position: "top",
          },
          title: {
            display: false,
            text: '',
          },
        },
      };
      
      const labels = ['', '', '', '', '', '', '', '', '', '', '', ''];
      
       const dataGraf = {
        labels,
        datasets: [
          {
            label: 'Dataset 1',
            data: [17, 7, 8, 16, 22, 17, 11, 18, 23, 8, 7, 25],
            backgroundColor: '#3682F7',
          },
        ],
      };



    return (
        <Bar options={options} data={dataGraf} width={140} height={100}/>
    )
}