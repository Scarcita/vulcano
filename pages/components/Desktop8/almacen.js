import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import ListSalidas from './listUltimasSalidas';
import ListRepuestos from './listRepuestosSolicitados';
import ListProducts from './listProductStock';
import BarChartGlobal from './BarChart';

/////////////// IMAGENES//////////////
import ImgItems from '../../../public/Desktop8/items.svg'
import ImgCostos from '../../../public/Desktop8/costo.svg'
import ImgRepuestos from '../../../public/Desktop8/repuestos.svg'

export default function Almacen() {

    return (
        <div className='flex h-full flex-col'>
            <div className='w-[1024px] mt-[36px] ml-[40px]'>
                <div className='flex flex-row'>
                    <h2 className="text-[24px] font-semibold">Almacen</h2>
                    <h2 className="text-[24px]">/Global</h2>
                </div>
                    <div className="mt-[20px] w-[1024px]">
                        <div className="flex flex-col">
                        <div className="flex flex-row">
                            <div className="w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px]">
                                <div className="flex flex-row">
                                    <div className="w-[169px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">Ingresos de productos</text>
                                        <div className="flex flex-row">
                                            <text className="text-[34px] text-[#3682F7] pl-[10px]">3490</text>
                                            <div className=' flex flex-row w-[44px] h-[20px] bg-[#3682F7] rounded-[5px] mt-[17px] ml-[8px] '>
                                                <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[6px] ml-[3px]'>
                                                    {/* <Image
                                                        src={Imgup}
                                                        layout='fixed'
                                                        alt='Imgup'
                                                    /> */}
                                                </div>
                                                <text className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">15%</text>
                                            </div>
                                        </div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Este mes</h3>
                                    </div>
                                    <div className=" h-[64px] bg-[#FFFFFF]">
                                        <BarChartGlobal/>
                                    </div> 
                                </div> 
                            </div>
                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px]">
                                <div className="flex flex-row">
                                    <div className="w-[169px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">Salidas por OT</text>
                                        <div className="flex flex-row">
                                            <text className="text-[34px] text-[#3682F7] pl-[10px]">3490</text>
                                            <div className=' flex flex-row w-[44px] h-[20px] bg-[#3682F7] rounded-[5px] mt-[17px] ml-[8px] '>
                                                <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[6px] ml-[3px]'>
                                                    {/* <Image
                                                        src={Imgup}
                                                        layout='fixed'
                                                        alt='Imgup'
                                                    /> */}
                                                </div>
                                                <text className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">15%</text>
                                            </div>
                                        </div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Este mes</h3>
                                    </div>
                                    <div className=" h-[64px] bg-[#FFFFFF]">
                                        <BarChartGlobal/>
                                    </div> 
                                </div> 
                            </div>
                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md">
                                <div className="flex flex-row">
                                    <div className="w-[169px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">Salidad por mostrador</text>
                                        <div className="flex flex-row">
                                            <text className="text-[34px] text-[#3682F7] pl-[10px]">3490</text>
                                            <div className=' flex flex-row w-[44px] h-[20px] bg-[#3682F7] rounded-[5px] mt-[17px] ml-[8px] '>
                                                <div className='w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[6px] ml-[3px]'>
                                                    {/* <Image
                                                        src={Imgup}
                                                        layout='fixed'
                                                        alt='Imgup'
                                                    /> */}
                                                </div>
                                                <text className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">15%</text>
                                            </div>
                                        </div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Este mes</h3>
                                    </div>
                                    <div className=" h-[64px] bg-[#FFFFFF]">
                                        <BarChartGlobal/>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                        <div className="flex flex-row">
                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px] mt-[24px] ">
                                <div className="flex flex-row">
                                    <div className="w-[166px] pl-[10px]">
                                        <text className="text-[14px] text-[#000000]pl-[10px]">#Items</text>
                                        <div className="text-[34px] text-[#3682F7] pl-[10px]">3490</div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">actual</h3>
                                    </div>
                                    <div className="w-[165px] pl-[70px]">
                                        <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full pl-[10px] pt-[16px] mt-[15px]">
                                            <Image
                                            src={ImgItems}
                                            layout='fixed'
                                            alt='ImgItems'
                                            /> 
                                        </div> 
                                    </div>
                                   
                                </div> 
                            </div>

                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px] mt-[24px]">
                                <div className="flex flex-row">
                                    <div className="w-[166px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">Costo Total</text>
                                        <div className="text-[34px] text-[#3682F7] pl-[10px]">3490</div>        
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Actual</h3>
                                    </div>
                                    <div className="w-[165px]  pl-[70px] ">
                                        <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full pl-[20px] pt-[16px] mt-[15px]">
                                            <Image
                                            src={ImgCostos}
                                            layout='fixed'
                                            alt='ImgCostos'
                                            /> 
                                        </div> 
                                    </div>
                                   
                                </div> 
                            </div>

                            <div className="flex flex-row w-[338px] h-[101px] bg-white rounded-[10px] pt-[8px] shadow-md mr-[16px] mt-[24px]">
                                <div className="flex flex-row">
                                    <div className="w-[166px]">
                                        <text className="text-[14px] text-[#000000] pl-[10px]">Solicitudes de repuesto</text>
                                        <div className="flex flex-row">
                                            <h1 className="text-[34px] text-[#A5A5A5] pl-[10px]">12/</h1>
                                            <h1 className="text-[34px] text-[#3682F7]">50</h1>
                                        </div>      
                                        <h3 className="text-[10px] text-[#A5A5A5] pl-[10px]">Codificadas vs Solicitudes</h3>
                                    </div>
                                    <div className="w-[165px]  pl-[70px] ">
                                        <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full pl-[15px] pt-[16px] mt-[15px]">
                                            <Image
                                            src={ImgRepuestos}
                                            layout='fixed'
                                            alt='ImgRepuestos'
                                            /> 
                                        </div> 
                                    </div>
                                   
                                </div> 
                            </div>

                        </div>
                        
                    </div>
                </div>
                <div className="flex flex-row">
                    <div className="w-[500px] mt-[15px] ">
                        <div className='flex flex-row'>
                            <h2 className="text-[24px] font-semibold">Ultimas salidas</h2>
                            <h2 className="text-[14px] mt-[12px]">/OTs y Mostrador</h2>
                        </div>

                        <div className='mt-[14px]'> 
                         <ListSalidas/>
                        </div>
                    </div>

                    <div className="w-[500px] ml-[24px]">
                        <div className="  mt-[15px] ">
                            <div className='flex flex-row'>
                                <h2 className="text-[24px] font-semibold">Repuestos solicitados</h2>
                                <h2 className="text-[14px] mt-[12px]">/En espera de codificacion</h2>
                            </div>
                            
                            <div className='mt-[14px]'> 
                             <ListRepuestos/>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <div className="w-[1024px] mt-[20px] ">
                <div className='flex flex-row ml-[40px]'>
                    <h2 className="text-[24px] font-semibold">Productos debajo del stock minimo</h2>
                </div>
                <div className='mt-[14px] ml-[40px]'> 
                    <ListProducts/>
                </div>

            </div>
            
            
        </div>
      
    );
  }




