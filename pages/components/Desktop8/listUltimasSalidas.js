import React, { useEffect, useState } from 'react';
import Head from "next/head"
import Image from 'next/image'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'

import ImgMedidor from '../../../public/Desktop8/medidor.svg'
import Img1904 from '../../../public/Desktop8/1904.svg'

export default function ListSalidas() {

    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState([]);

    useEffect(() => {
  
        setTimeout(function(){
          fetch('https://api.npms.io/v2/search?q=react')
            .then(response => response.json())
            .then(data => setTotal(data.total))
            .then(setShowDots(false));
        }, 2000);
        
      }, [])
    
    const tableSalidas = [
        {
            id: 0,
            total: '45',
            nombre: 'GOBIERNOS AUTONOMO MUNICIPAL',
            placa: '27349SFD',
            type: 'medidor'
        },
        {
            id: 1,
            total: '46',
            nombre: 'GOBIERNOS AUTONOMO MUNICIPAL',
            placa: '27349SFD',
            type: '1904'
        },

        {
            id: 2,
            total: '47',
            nombre: 'GOBIERNOS AUTONOMO MUNICIPAL',
            placa: '27349SFD',
            type: 'medidor'
        },
        {
            id: 3,
            total: '48',
            nombre: 'GOBIERNOS AUTONOMO MUNICIPAL',
            placa: '27349SFD',
            type: '1904'
        },
    

        ];

    return (
        showDots ? 
        <div className='flex justify-center items-center w-[500px] mt-[40px]'  > 
          <Spinner color="#bebebe" size={17} speed={1} animating={true} style={{marginLeft: 'auto', marginRight: 'auto'}} />
        </div> 
        :
        <div>
            <Table tableSalidas={tableSalidas} />
        </div>
    )
}



const Table = (props) => { 

    const { tableSalidas } = props;
    
        return ( 
    
            <table className={` w-[500px] h-[282px] bg-[#FFFFFF] rounded-[24px]`}> 
                <tbody> 
                    {tableSalidas.map(row => <TableRow row={row} />)} 
                </tbody>
            </table> 
    
        ); 
    };
    
    
    
    
class TableRow extends React.Component{ 

    render() { 

        let row = this.props.row; 

        return ( 
            <div className="flex flex-row pl-[15px]">
                <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full mt-[10px] pt-[12px] pl-[13px]">
                    { 
                        row.type === 'medidor'?
                        <Image
                        src={ImgMedidor}
                        layout='fixed'
                        alt='ImgMedidor'
                        /> 
                        : <></>
                    }
                        { 
                        row.type === '1904'?
                        <Image
                        src={Img1904}
                        layout='fixed'
                        alt='Img1904'
                        /> 
                        : <></>
                    }
                </div>

                <div className=" ml-[20px] mt-[5px]">
                    <tr className="text-[40px] font-semibold text-[#FF0000]">{row.total}</tr>
                </div>

                <div className=" ml-[20px] mt-[15px]">
                    <tr className="text-[14px] text-[#000000] font-bold">{row.nombre}</tr>
                    <tr className="text-[16px] text-[#000000]"> {row.placa}</tr>  
                </div>
                    
                
                
            </div>
        ) 

    } 
};
