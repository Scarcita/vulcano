import React, { useEffect, useState } from 'react';
import Image from 'next/image'

import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'

import ImgItems from '../../../public/Desktop8/items.svg'


export default function ListProducts() {

    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState([]);

    useEffect(() => {
  
        setTimeout(function(){
          fetch('https://api.npms.io/v2/search?q=react')
            .then(response => response.json())
            .then(data => setTotal(data.total))
            .then(setShowDots(false));
        }, 2000);
        
      }, [])
      
    //////////////// TABLA TRES ///////////////

    const tableStock = [
        {
            id: 0,
            placa: '27349SFD',
            nombre: 'GOBIERNOS AUTONOMO MUNICIPAL',
            stockMin: '42',
            stockMax: '45',
            type: 'medidor'
        },

        {
            id: 1,
            placa: '27349SFD',
            nombre: 'GOBIERNOS AUTONOMO MUNICIPAL',
            stockMin: '42',
            stockMax: '45',
            type: 'medidor'
        },

        {
            id: 2,
            placa: '27349SFD',
            nombre: 'GOBIERNOS AUTONOMO MUNICIPAL',
            stockMin: '42',
            stockMax: '45',
            type: 'medidor'
        },

        {
            id: 3,
            placa: '27349SFD',
            nombre: 'GOBIERNOS AUTONOMO MUNICIPAL',
            stockMin: '42',
            stockMax: '45',
            type: 'medidor'
        },

        ];


    return (
        showDots ? 
        <div className='flex justify-center items-center w-[1024px] mt-[40px]'  > 
          <Spinner color="#bebebe" size={17} speed={1} animating={true} style={{marginLeft: 'auto', marginRight: 'auto'}} />
        </div> 
        :
         <Table3 tableStock={tableStock}/>
    )
}


///////////////// TABLE TRES //////////

const Table3 = (props) => { 

    const { tableStock } = props;
    
        return ( 
    
            <table className={` w-[1024px] h-[275px] bg-[#FFFFFF] rounded-[24px]`}> 
                <tbody>
                    <div className="flex flex-row mt-[10px] mb-[5px]">
                        <div className="w-[700px] pl-[24px]">
                            <text className="text-[16px] text-[#A5A5A5]">Producto</text>
                        </div>
                        <div className="w-[160px] text-center">
                            <text className="text-[16px] text-[#A5A5A5]">Stock Min</text>
                        </div>
                        <div className="w-[160px] text-center ">
                            <text className="text-[16px] text-[#A5A5A5]">Stock Actual</text>  
                        </div>
                    </div>
                    <div className="w-[1000px] border-[1px] ml-[14px]"></div>
                    {tableStock.map(row => <TableRow3 row={row} />)} 
                </tbody>
            </table> 
    
        ); 
    };
    
    
    
        
class TableRow3 extends React.Component{ 

    render() { 
        let row = this.props.row; 

        return ( 
            <div className="flex flex-row pl-[15px]">
                <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full mt-[5px] mb-[5px] pt-[15px] pl-[10px]">
                    { 
                        row.type === 'medidor'?
                        <Image
                        src={ImgItems}
                        layout='fixed'
                        alt='ImgItems'
                        /> 
                        : <></>
                    }
                        { 
                        row.type === '1904'?
                        <Image
                        src={ImgItems}
                        layout='fixed'
                        alt='ImgItems'
                        /> 
                        : <></>
                    }
                </div>

                <div className="w-[90px] pl-[20px] pt-[20px]">
                    <tr className="text-[16px] text-[#000000]">{row.placa}</tr>
                </div>
                <div className="w-[540px] ml-[20px] pt-[20px]">
                    <tr className="text-[16px] text-[#000000] font-bold">{row.nombre}</tr>
                </div>
                <div className="w-[150px] pl-[30px]">
                    <tr className="text-[40px] text-[#000000] font-semibold"> {row.stockMin}</tr>
                </div>
                <div className="w-[150px] pl-[30px]">
                    <tr className="text-[40px] text-[#FF0000] font-semibold"> {row.stockMax}</tr>
                </div>
            </div>
        ) 

    } 
};