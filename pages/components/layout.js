import Link from 'next/link';
import Image from 'next/image'
import {useRouter} from 'next/router';

import ImgLogo from '../../public/logo_azul.png'
import ImgCar from '../../public/car-2.png';
import ImgCarRepair from '../../public/car-repair.png'


export default function Layout({ children }) {
    const router = useRouter();
    

    const menuItems = [
        // {
        //   href: '/',
        //   title: 'Signin',
        //   type: 'home'
        // },
        {
          href: '/components/Desktop/nuevaOrden',
          title: 'Nueva Orden',
          type: 'home'
        },
        // {
        //   href: '/citas',
        //   title: 'Citas',
        //   type: 'citas'
        // },
        // {
        //   href: '/asignar',
        //   title: 'Asignar SRs',
        //   type: 'asignar'
        // },
        // {
        //   href: '/pantallaCinco',
        //   title: 'Orden trabajo',
        //   type: 'asignar'
        // },
        {
          href: '/components/Desktop5/desktop5',
          title: 'Work Global',
          type: 'asignar'
        },
        {
          href: '/components/Desktop8/almacen',
          title: 'Almacen',
          type: 'asignar'
        },
        {
          href: '/components/Desktop12/facturacion',
          title: 'Facturacion',
          type: 'asignar'
        },
      ];


    return (
        <div className="min-h-screen flex flex-col bg-gris">
            <div className="flex flex-col md:flex-row flex-1">
            <aside className='bg-[#FCFDFE] w-full md:w-60'>
                <nav>
                  <div className='text-center mt-[85px] mb-[115px]'>
                  <Image
                      src={ImgLogo}
                      layout='fixed'
                      alt='ImgLogo'
                  />
                  </div>
                    <ul>
                    {menuItems.map(({ href, title, type }) => (
                      
                        <li key={title} >
                        <Link href={href} >
                            <a
                            className={
                              `ml-[55px] mb-[40px] flex flex-col bg-[#F6F6FA] w-[122px] h-[114px] pt-[25px] rounded-[25px] text-[12px] text-center hover:bg-[#3682F7] hover:text-[#FFFFFF] shadow hover:shadow-xl cursor-pointer ${router.asPath === href && 'bg-[#3682F7] text-white'}`}                            
                            >
                              <div>
                                { 
                                  type === 'home'?
                                  <Image
                                  src={ImgCar}
                                  layout='fixed'
                                  alt='ImgCar'/> 
                                    : <></>
                                }
                                { 
                                  type === 'citas'?
                                  <Image
                                  src={ImgCarRepair}
                                  layout='fixed'
                                  alt='ImgCarRepair'
                                  /> 
                                    : <></>
                                }
                                { 
                                  type === 'asignar'?
                                  <Image
                                  src={ImgCar}
                                  layout='fixed'
                                  alt='ImgCar'
                                  /> 
                                    : <></>
                                }

                                
                              
                              </div>
                              {title}
                            </a>
                        </Link>
                        </li>
                    ))}
                    </ul>
                </nav>
            </aside>
                <main className="flex-1">{children}</main>
            </div>

         
        </div>  
    );
  }