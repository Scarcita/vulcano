import React, { useEffect, useState } from 'react';
import 'react-calendar/dist/Calendar.css';
import Modal from './Modal'

import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'


export default function ListObservacionesCli() {
    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState([]);

    useEffect(() => {
  
        setTimeout(function(){
          fetch('https://api.npms.io/v2/search?q=react')
            .then(response => response.json())
            .then(data => setTotal(data.total))
            .then(setShowDots(false));
        }, 2000);
        
      }, [])

  /////////////////// LISTA TABLA UNO/////////////////////

  const [description, setDescription] = useState("");

  const handleinput = (e) => {
    console.log("input: " + e.target.value);
    setDescription(e.target.value);
  };

  const HandleSubmit = (e) => {
    console.log("submit: " + description);
    var bodyTemp = listBody;
    bodyTemp.push({id: listBody.length, val: description});
    setListBody([...bodyTemp]);
    console.log(bodyTemp);
  };

  const heading = ['Descripcion']; 

  const body =[
    {
      id: 0,
      val: 'MANTENIMIENTO AUDI-10000 KM'
    },

    {
      id: 1,
      val: 'MANTENIMIENTO AUDI-20000 KM'
    },
    {
      id: 2,
      val: 'MANTENIMIENTO AUDI-0000 KM'
    },
    {
      id: 3,
      val: 'MANTENIMIENTO AUDI-10000 KM'
    },
    {
      id: 4,
      val: 'MANTENIMIENTO AUDI-10000 KM'
    },
    
  ]

  const [listBody, setListBody] = useState(body); 

      useEffect(() => {
        console.log("descriptopn changed: " + description);
      }, [description])


      return(
        showDots ? 
        <div className='flex justify-center items-center w-[722px] mt-[40px]'  > 
          <Spinner color="#bebebe" size={17} speed={1} animating={true} style={{marginLeft: 'auto', marginRight: 'auto'}} />
        </div> 
        : 
        <div>
            
            <div className="flex flex-row justify-between mt-[32px]">
                <div className='text-[17px]'>Observacion del cliente</div>
                <div>
                <input className="w-[380px] h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[4px] pl-[5px] text-[12px] mr-[10px]"
                type="text"
                value={description}
                onChange={(e) => handleinput(e)}
                />
                <button className="w-[72px] h-[25px]  border-[1px] border-[#3682F7] rounded-[7px] text-blue hover:bg-blue hover:text-white text-[13px]"
                onClick={() => HandleSubmit()}
                > Anadir</button>
                </div>
            </div>

            <div style={{maxHeight: '180px', overflowY: 'auto'}} className="mt-3">
                <div > 
                    <Table heading={heading} body={listBody} />
                </div> 
            
            </div>

      </div>

      )
    }


    
 //////////////////TABLE UNO ///////////////////


 const Table = (props) => { 

    const {heading, body } = props;
  
      return ( 
  
          <table className={`pt-1 pl-5 w-[720px] bg-[#FCFDFE] rounded-[10px] shadow-md`}> 
              <thead> 
                  <tr> 
                      {heading.map(head => 
                      <th style={{position: 'sticky', top: 0}} className="h-[43px] bg-blue text-white font-[14px]">{head}</th>)} 
                  </tr> 
              </thead> 
              <tbody>
                <div className="grid grid-cols-1 divide-y w-[710px] pl-3">
                  {body.map(row => <TableRow row={row} />)}      
                </div> 
              </tbody> 
  
          </table> 
      );   
  } 

  
  //////////////////////TABLE ROW UNO /////////////////////
class TableRow extends React.Component{ 

    render() { 
    
      function handleDelete(id) {
        console.log(id);
      };
    
        let row = this.props.row; 
        console.log(row.id);
    
        var counter = 0;
        
    
        return (   
            <tr>
              <div className='flex flex-row justify-between'>
                <td className="text-[10px] pt-1 pb-1 text-black">{row.val}</td>
                <div className='flex '>
                  <Modal id={row.id} body={row.val} />
                  <button className="w-[20px] h-[20px] border-[1px] border-red-600 rounded-[6px] text-red-600 hover:bg-red-600 hover:text-white text-[10px] mt-[3px] ml-[10px] "id={row.id}
                  onClick={() => handleDelete (row.id)}>X</button>
                  
                </div>
              </div>
            </tr>   
        ) 
    } 
    }

    