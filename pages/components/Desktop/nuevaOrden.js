import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import styles from '../../../styles/Home.module.css'
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import ListObservacionesCli from '../../../pages/components/Desktop/listObservaciones';
import ListServiceReq from '../../../pages/components/Desktop/listServiciosReq';
import ListServices from '../../../pages/components/Desktop/listServices';


export default function NuevaOrden() {

  const [value, onChange] = useState(new Date());
  const [descripcion, onChangeDescripcion] = React.useState("");

  const validationSchema = Yup.object().shape({
    marca: Yup.string()
    .required('marca is required'),
    modelo: Yup.string()
    .required("modelo is required")
    .matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    version: Yup.string()
    .required('version is required')
    .matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    color: Yup.string()
    .required('color is required')
    .matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    placa: Yup.number()
    .required('placa is required')
    .min(6, 'minimo 6 caracteres'),
    //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),

    vin: Yup.string()
    .required('vin is required'),

    kilometraje: Yup.string()
    .required('kilometraje is required'),

    transmision: Yup.string()
    .required('transmision is required'),

    nombre: Yup.string()
    .required('nombre is required')
    .matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    telefono: Yup.string()
    .required('telefono is required')
    .matches(/^[\(]?[\+]?(\d{2}|\d{3})[\)]?[\s]?((\d{6}|\d{8})|(\d{3}[\*\.\-\s]){3}|(\d{2}[\*\.\-\s]){4}|(\d{4}[\*\.\-\s]){2})|\d{8}|\d{10}|\d{12}$/, 'Ingrese un numero valido'),

    email: Yup.string()
      .required('Email is required')
      .email('Email is invalid'),

    nombre2: Yup.string()
    .required('nombre is required')
    .matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    nit: Yup.string()
    .required('nit is required')
    .matches(/[A-za-z0-9]/, 'solo se permite numeros'),

    nombre3: Yup.string()
    .required('nombre is required')
    .matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    celular: Yup.string()
    .required('celular is required')
    .matches(/^[\(]?[\+]?(\d{2}|\d{3})[\)]?[\s]?((\d{6}|\d{8})|(\d{3}[\*\.\-\s]){3}|(\d{2}[\*\.\-\s]){4}|(\d{4}[\*\.\-\s]){2})|\d{8}|\d{10}|\d{12}$/, 'Ingrese un numero valido'),
  })

  const formOptions = { resolver: yupResolver(validationSchema)};
  const {register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  function onSubmit(data) {
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
      return false;
      
  }


  return (
    <div className='flex h-full flex-col'>
      <form onSubmit={handleSubmit(onSubmit)}>
      <div className='w-[1024px] mt-[36px] '>
        <div className='flex flex-row ml-[40px] justify-between'>
          <h1 className='text-[32px] text-[#000000] '>Nueva Orden</h1>
            <div className='flex flex-row text-right'>
              <div>
                <span className='text-[15px]  text-[#000000]' >Juan Camacho</span>
                <h1 className='text-[15px] text-[#ACB5BD]' >ASESOR DE SERVICIO</h1>
              </div>
              <div className='w-[62px] h-[62px] rounded-full bg-[#E4E7EB] ml-5'></div>
            </div>
        </div >
        

        <div className={`flex flex-row w-[1024px] h-[63px] bg-blue rounded-[10px] shadow-md mt-[36px] ml-[40px] pt-1 pl-5 justify-evenly`}>

            <div>
              <div className={styles.list}>Marca</div>
              <input className="w-[110px] h-[17px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                 {...register('marca')}
                 type="text"
                 name="marca"
                 placeholder='Descripcion' 
                />
                <div className="b-[#EE002B]">{errors.marca?.message}</div>
            </div>

            <div>
              <div className={styles.list}>Modelo</div>
              <input className="w-[87px] h-[17px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                  {...register('modelo')}
                  type="text"
                  name="modelo"
                  placeholder='Descripcion' 
                 />
                 <div className="b-[#EE002B]">{errors.modelo?.message}</div>
            </div>

            <div>
              <div className={styles.list}>Version</div>
              <input className="w-[87px] h-[17px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                  {...register('version')}
                  type="text"
                  name="version"
                  placeholder='Descripcion' 
                 />
                 <div className="b-[#EE002B]">{errors.version?.message}</div>
            </div>

            <div>
              <div className={styles.list}>Color</div>
              <input className="w-[87px] h-[17px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                 {...register('color')}
                 type="text"
                 name="color"
                 placeholder='Descripcion' 
                />
                <div className="b-[#EE002B]">{errors.color?.message}</div>
            </div>

            <div>
              <div className={styles.list}>Placa</div>
              <input className="w-[87px] h-[17px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                {...register('placa')}
                type="text"
                name="placa"
                placeholder='Descripcion' 
               />
               <div className="b-[#EE002B]">{errors.placa?.message}</div>
            </div>

            <div>
              <div className={styles.list}>VIN</div>
              <input className="w-[110px] h-[17px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                 {...register('vin')}
                 type="text"
                 name="vin"
                 placeholder='Descripcion' 
                />
                <div className="b-[#EE002B]">{errors.vin?.message}</div>
            </div>

            <div>
              <div className={styles.list}>Kilometraje</div>
              <input className="w-[87px] h-[17px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                 {...register('kilometraje')}
                 type="text"
                 name="kilometraje"
                 placeholder='Descripcion' 
                />
                <div className="b-[#EE002B]">{errors.kilometraje?.message}</div>
            </div>

            <div>
              <div className={styles.list}>Transmision</div>
              <input className="w-[87px] h-[17px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                 {...register('transmision')}
                 type="text"
                 name="transmision"
                 placeholder='Descripcion' 
                />
                <div className="b-[#EE002B]">{errors.transmision?.message}</div>
            </div>
          </div>
          
          
          <div className="flex flex-row mt-[20px] ml-[40px] w-[1024px] justify-between">
            <div className="flex flex-row w-[446px] h-[63px] bg-white rounded-[10px] pt-2 shadow-md justify-evenly">
            <div>
              <div className="text-[12px] text-black">Nombre</div>
              <input className="w-[150px] h-[17px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                {...register('nombre')}
                type="text"
                name="nombre"
                placeholder='Descripcion' 
               />
               <div className="b-[#EE002B]">{errors.nombre?.message}</div>
            </div>

            <div>
              <div className="text-[12px] text-black">Telefono</div>
              <input className="w-[87px] h-[17px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                 {...register('telefono')}
                 type="text"
                 name="telefono"
                 placeholder='Descripcion' 
                />
                <div className="b-[#EE002B]">{errors.telefono?.message}</div>
            </div>

            <div>
              <div className="text-[12px] text-black">Email</div>
              <input className="w-[150px] h-[17px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                 {...register('email')}
                 type="text"
                 name="email"
                 placeholder='Descripcion' 
                />
                <div className="b-[#EE002B]">{errors.email?.message}</div>
            </div>

            </div>

            <div className="flex flex-row w-[243px] h-[63px]  bg-white rounded-[10px]  shadow-md pt-2 justify-evenly">

            <div>
              <div className="text-[12px] text-black">Nombre</div>
              <input className="w-[120px] h-[17px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                {...register('nombre2')}
                type="text"
                name="nombre2"
                placeholder='Descripcion' 
               />
               <div className="b-[#EE002B]">{errors.nombre2?.message}</div>
            </div>

            <div>
              <div className="text-[12px] text-black">NIT/C.I.</div>
              <input className="w-[87px] h-[17px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                {...register('nit')}
                type="text"
                name="nit"
                placeholder='Descripcion' 
               />
               <div className="b-[#EE002B]">{errors.nit?.message}</div>
            </div>

            </div>

            <div className="flex flex-row w-[273px] h-[63px] bg-white rounded-[10px]  shadow-md pt-2 justify-evenly">

            <div>
              <div className="text-[12px] text-black">Nombrw</div>
              <input className="w-[150px] h-[17px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                {...register('nombre3')}
                type="text"
                name="nombre3"
                placeholder='Descripcion' 
               />
               <div className="b-[#EE002B]">{errors.nombre3?.message}</div>
            </div>

            <div>
              <div className="text-[12px] text-black">Celular</div>
              <input className="w-[87px] h-[17px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[9px] pl-[2px]"
                {...register('celular')}
                type="text"
                name="celular"
                placeholder='Descripcion' 
               />
               <div className="b-[#EE002B]">{errors.celular?.message}</div>
            </div>
            </div>
          </div>

      </div>
       
          <div className="w-[722px] flex flex-row">
            <div className="flex-col">
              <div className="flex flex-row ml-[40px]">
                <div>
                  <ListObservacionesCli/>

                </div>
              </div>
              <div className="flex flex-row ml-[40px]">
                <div>
                  <ListServiceReq/>

                </div>
               
              </div>

            </div>
              <div className="w-[280px]">

                <div>
                    <div className='text-[17px] text-[#000000] ml-[32px] mt-[32px]'>Promesa de entraga</div>
                    <div className="ml-[32px] mt-[18px]">
                      <Calendar
                          onChange={onChange} 
                          value={value}
                      />
                    </div>
                </div>

                <div>
                    <div className='text-m font-semibold ml-[32px] mt-[20px]'>Comentarios adicionales</div>
                    <textarea className="w-[273px] h-[80px] ml-[32px] border-[1px] border-[#E4E7EB] rounded-[10px] mt-3 pl-[5px]"/>
                    <button className="w-[273px] h-[57px] ml-[32px] rounded-[7px] text-white bg-blue mt-[21px"
                    type="submit" 
                    > Guardar</button>
                </div>

            </div>
            
            
          </div>
          </form>
          

        {/* <div className="flex flex-row mt-[40px] ml-[40px]">
          <div style={{maxHeight: '180px', overflowY: 'auto'}}>
            <div > 
                <ListServices/>
            </div>          
          </div>
        </div> */}
    </div>
    
  );
}


