import React, { useEffect, useState } from 'react';
import 'react-calendar/dist/Calendar.css';
import Modal from './Modal'
import { ReactSearchAutocomplete } from 'react-search-autocomplete'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'


export default function ListServiceReq() {

  const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState([]);

    useEffect(() => {
  
        setTimeout(function(){
          fetch('https://api.npms.io/v2/search?q=react')
            .then(response => response.json())
            .then(data => setTotal(data.total))
            .then(setShowDots(false));
        }, 2000);
        
      }, [])


////////////////////// LISTA TABLA DOS /////////////////////

const [descServicios, setDescServicios] = useState("");

const asigInput = (e) => {
  console.log("input: " + e.target.value);
  setDescServicios(e.target.value);
};

const asigSubmit = (e) => {
  console.log("submit: " + descServicios);
  var bodyTempo = listservicios;
  bodyTempo.push({id: listservicios.length, val: descServicios});
  setListSErvicios([...bodyTempo]);
  console.log(bodyTempo);
};


const headServicios = ['Descripcion']; 

const bodyServicios =[
  {
    id: 0,
    val: 'AUDI-10000 KM'
  },

  {
    id: 1,
    val: 'AUDI-20000 KM'
  },
  {
    id: 2,
    val: 'AUDI-30000 KM'
  },
  {
    id: 3,
    val: 'AUDI-40000 KM'
  },
  {
    id: 4,
    val: 'AUDI-50000 KM'
  },
  
]


const [listservicios, setListSErvicios] = useState(bodyServicios); 

useEffect(() => {
console.log("descriptopn changed: " + descServicios);
}, [descServicios])


const handleOnSearch = (string, results) => {
// onSearch will have as the first callback parameter
// the string searched and for the second the results.
console.log(string, results)
}

const handleOnHover = (result) => {
// the item hovered
console.log(result)
}

const handleOnSelect = (item) => {
// the item selected
console.log(item)
}

const handleOnFocus = () => {
console.log('Focused')
}

const formatResult = (item) => {
return (
  <>
    <span style={{ display: 'block', textAlign: 'left' }}>id: {item.id}</span>
    <span style={{ display: 'block', textAlign: 'left' }}>name: {item.val}</span>
  </>
)
}

      return(
        showDots ? 
        <div className='flex justify-center items-center w-[720px] mt-[40px]'  > 
            <Spinner color="#bebebe" size={17} speed={1} animating={true} style={{marginLeft: 'auto', marginRight: 'auto'}} />
        </div> 
        :
        <div>
            <div className="flex flex-row justify-between mt-[40px] ">
                <div className='text-[17px]'>Observacion del cliente</div>
                {/* <div>
                    <input className="w-[380px] h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[4px] pl-[5px] text-[12px] mr-[10px]"
                    type="text"
                    value={descServicios}
                    onChange={(e) => asigInput(e)}
                    />
                    <button className="w-[72px] h-[25px]  border-[1px] border-[#3682F7] rounded-[7px] text-blue hover:bg-blue hover:text-white text-[13px]"
                    onClick={() => asigSubmit ()}
                    > Anadir</button>
                </div> */}
                <header className="App-header">
                    <div style={{ width: 400 }}>
                        <ReactSearchAutocomplete
                        items={listservicios}
                        onSearch={handleOnSearch}
                        onHover={handleOnHover}
                        onSelect={handleOnSelect}
                        onFocus={handleOnFocus}
                        autoFocus
                        formatResult={formatResult}
                        />
                    </div>
                </header>
            </div>

            {/* <div style={{maxHeight: '180px', overflowY: 'auto'}} className="mt-3">
                <div > 
                    <Table2 headServicios={headServicios} bodyServicios={listservicios} />
                </div>          
            </div> */}

        </div>

      )
    }



//////////////////TABLE DOS ///////////////////

const Table2 = (props) => { 

    const {headServicios, bodyServicios } = props;
    
      return ( 
    
          <table className={` pt-1 pl-5 w-[720px] bg-[#FCFDFE] rounded-[10px] shadow-md`}> 
              <thead> 
                  <tr> 
                      {headServicios.map(head => 
                      <th style={{position: 'sticky', top: 0}} className="h-[43px] bg-blue text-white font-[14px]">{head}</th>)} 
                  </tr> 
              </thead> 
              <tbody>
                <div className="grid grid-cols-1 divide-y w-[710px] pl-3">
                  {bodyServicios.map(row => <TableRow2 row={row} />)}      
                </div> 
              </tbody> 
    
          </table> 
      );   
    } 
  

//////////////////////TABLE ROW DOS /////////////////////
class TableRow2 extends React.Component{ 

    render() { 
    
      function handleDelete2(id) {
        console.log(id);
      };
    
        let row = this.props.row; 
        console.log(row.id);
    
        var counter = 0;
    
        return ( 
     
          <tr>
            <div className='flex flex-row justify-between'>
         
              <td className="text-[10px] pt-1 pb-1 text-black">{row.val}</td>
              
              <div className='flex '>
                <Modal id={row.id} body={row.val} />
                <button className="w-[20px] h-[20px] border-[1px] border-red-600 rounded-[6px] text-red-600 hover:bg-red-600 hover:text-white text-[10px] mt-[3px] ml-[10px] "id={row.id}
                onClick={() => handleDelete2 (row.id)}>X</button>
              </div>
            </div>
          </tr>   
        ) 
    } 
    }

    