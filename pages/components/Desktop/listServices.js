import React, { useEffect, useState } from 'react';
import 'react-calendar/dist/Calendar.css';
import Modal from './Modal'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'

export default function ListServices() {

  
  const [showDots, setShowDots] = useState(true);
  const [total, setTotal] = useState([]);

  useEffect(() => {

      setTimeout(function(){
        fetch('https://api.npms.io/v2/search?q=react')
          .then(response => response.json())
          .then(data => setTotal(data.total))
          .then(setShowDots(false));
      }, 2000);
      
    }, [])



  //////////////////////// LISTA TABLA TRES ///////////////////////

  const [ServiceReq, SetServiceReq] = useState("");
    const headRequiredService = [
      'Nombre', "Precio ref.", "Precio hr.", 'Hrs. Hombre', "Cliente", "Empresa", 'Garantia', "Mecanico", ""
    ]; 

    const bodyRequiredService =[
      {
        id: 0,
        val: 'AUDI-10000 KM',
        precio: '1000',
        precioHora: '100',
        hombre: '24',
        cliente: 'Jhon',
        empresa: 'xxxxxx',
        garantia: 'xxxxx',
        mecanico: 'julio'
      },

      {
        id: 1,
        val: 'AUDI-10000 KM',
        precio: '1000',
        precioHora: '100',
        hombre: '24',
        cliente: 'Mario',
        empresa: 'xxxxxx',
        garantia: 'xxxxx',
        mecanico: 'Gustavo'
      },

      {
        id: 2,
        val: 'AUDI-10000 KM',
        precio: '1000',
        precioHora: '100',
        hombre: '24',
        cliente: 'Roberto',
        empresa: 'xxxxxx',
        garantia: 'xxxxx',
        mecanico: 'Ruben'
      },

      {
        id: 3,
        val: 'AUDI-10000 KM',
        precio: '1000',
        precioHora: '100',
        hombre: '24',
        cliente: 'Miguel',
        empresa: 'xxxxxx',
        garantia: 'xxxxx',
        mecanico: 'Josue'
      },

      {
        id: 4,
        val: 'AUDI-10000 KM',
        precio: '1000',
        precioHora: '10',
        hombre: '24',
        cliente: 'Daniel',
        empresa: 'xxxxxx',
        garantia: 'xxxxx',
        mecanico: 'Valeria'
      },
      
    ]

    const [listRequiredService, setListRequiredService] = useState(bodyRequiredService); 

    useEffect(() => {
      console.log("descriptopn changed: " + ServiceReq);
    }, [ServiceReq])


      return(
        showDots ? 
        <div className='flex justify-center items-center w-[345px] mt-[40px]'  > 
            <Spinner color="#bebebe" size={17} speed={1} animating={true} style={{marginLeft: 'auto', marginRight: 'auto'}} />
        </div> 
        :
        <div>
          <Table3 headRequiredService={headRequiredService} bodyRequiredService={listRequiredService} />

        </div>

      )
    }



//////////////////////TABLE TRES ///////////////////

const Table3 = (props) => { 

    const {headRequiredService , bodyRequiredService } = props;
    
      return ( 
    
          <table className={` pt-1 pl-5 w-[740px] bg-[#FCFDFE] rounded-[10px] shadow-md`}> 
              <thead> 
                  <tr> 
                      {headRequiredService.map(head => 
                      <th style={{position: 'sticky', top: 0}} className="h-[43px] bg-blue text-white font-[12px]">{head}</th>)} 
                  </tr> 
              </thead> 
              <tbody>
                {/* <div className="grid grid-cols-1 divide-y w-[730px] pl-3"> */}
                  {bodyRequiredService.map(row => <TableRow3 row={row} />)}      
                {/* </div>  */}
              </tbody> 
    
          </table> 
      );   
    } 
    
    
    
    

//////////////////////TABLE ROW TRES /////////////////////
class TableRow3 extends React.Component{ 

render() { 

    function handleDelete3(id) {
    console.log(id);
    };

    let row = this.props.row; 
    console.log(row.id);

    var counter = 0;

    return ( 
    
        <tr>

            <td className="w-[100px] text-[10px] pt-1 pb-1 text-black pl-[20px]">{row.val}</td>
            <td className="w-[80px] text-[10px] text-black">{row.precio}</td>
            <td className="w-[80px] text-[10px] text-black">{row.precioHora}</td>
            <td className="w-[80px] text-[10px] text-black">{row.hombre}</td>
            <td className="w-[80px] text-[10px] text-black">{row.cliente}</td>
            <td className="w-[80px] text-[10px] text-black">{row.empresa}</td>
            <td className="w-[80px] text-[10px] text-black">{row.garantia}</td>
            <td className="w-[80px] text-[10px] text-black">{row.mecanico}</td>
            <td className=" flex flex-row-[80px] text-[10px] pt-1 pb-1 text-black">
                
            <Modal id={row.id} body={row.val} />
            <button className="w-[20px] h-[20px] border-[1px] border-red-600 rounded-[4px] text-red-600 hover:bg-red-600 hover:text-white text-[10px] mt-[3px] ml-[10px] "id={row.id}
            onClick={() => handleDelete3 (row.id)}>X</button>
                
                </td>
                

        </tr>   
        
    ) 
} 
}