import { useState } from "react";
import Image from 'next/image'

import ImgEdit from '../../../public/editar.svg'

export default function Modal(props) {


    const [showModal, setShowModal] = useState(false);

    const [description, setDescription] = useState("");

    const {id, body} = props;
    


    return (
        <>
            <div>
                <button
                    className="w-[20px] h-[20px] border-[1px] border-[#3682F7] rounded-[6px]  hover:bg-blue hover:text-white mt-[3px]"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    <Image
                    src={ImgEdit}
                    style={{position: 'absolute'}}
                    alt='ImgEdit'
                />
                </button>
            </div>
            {showModal ? (
                 <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                         className="fixed inset-0 w-full h-full bg-black opacity-40"
                         onClick={() => setShowModal(false)}
                     ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-white rounded-md shadow-lg">
                 
                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                            Edit payment Method
                                    </h4>
                                    
                                </div>

                                <div className='mt-[20px]'>
                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Descripcion</p>
                                    <input className="w-[570px] h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                    type="text"
                                    value={body}
                                    onChange={(e) => body(e)}
                                    />
                                </div>

                                <div className='mt-[20px]'>
                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Credit/debit card number</p>
                                    <input className="w-[570px] h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                    type="text"
                                    value={description}
                                    onChange={(e) => setDescription(e)}
                                    />
                                </div>

                                <div className="flex flex-row mt-[20px]">
                                    <div>
                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Expiration month and year</p>
                                        <input className="w-[280px] h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px]"
                                        type="text"
                                        value={description}
                                        onChange={(e) => setDescription(e)}
                                        />
                                    </div>
                                    <div>
                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>CVC</p> 
                                        <input className="w-[280px] h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                        type="text"
                                        value={description}
                                        onChange={(e) => setDescription(e)}
                                        />
                                    </div>
                                    
                                </div>


                                <div className="flex flex-row justify-between mt-[20px]">

                                    <div>
                                        <h1 className="text-[12px] mt-[10px]">This field is mandatory</h1>
                                    </div>

                                    <div>
                                        <button
                                            className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-white text-[14px] mt-[3px] mr-[10px]"
                                            onClick={() =>
                                                setShowModal(false)
                                            }
                                        >
                                            Cancel
                                        </button>
                                        <button
                                            className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                            onClick={() =>
                                                setShowModal(false)
                                            }
                                        >
                                            Update
                                        </button>
                                    </div>
                                </div>
                                    
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}